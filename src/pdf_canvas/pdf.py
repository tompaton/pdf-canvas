# ruff: noqa: N802 N803
import io
from collections.abc import Iterator, Sequence
from contextlib import contextmanager
from enum import Enum
from fractions import Fraction
from typing import Any, Optional, cast

from reportlab import platypus
from reportlab.lib import colors
from reportlab.lib.enums import TA_CENTER, TA_LEFT, TA_RIGHT
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import cm, inch
from reportlab.pdfgen import canvas

Color = type[colors.Color]
ColorRGB = tuple[int, int, int]


def get_color(color: str, alpha: float | None = 1.0) -> Color:
    if color == "black":
        return cast(Color, colors.Color(0, 0, 0, alpha))

    return cast(Color, getattr(colors, color))


def get_color_rgb(color: str) -> ColorRGB:
    return cast(ColorRGB, get_color(color).bitmap_rgb())


class Align(Enum):
    Left = TA_LEFT
    Center = TA_CENTER
    Right = TA_RIGHT


def cm_to_pixels(v: float | Fraction, dpi: float) -> int:
    return int((v * cm / inch) * dpi)


class Canvas:
    def __init__(self, width: Fraction, height: Fraction) -> None:
        self.output = io.BytesIO()
        self.ctx = canvas.Canvas(self.output, pagesize=(width * cm, height * cm))

    def __getattr__(self, attr: str) -> Any:
        if attr not in [
            "showPage",
            "setStrokeColor",
            "setFillColor",
            "setLineCap",
            "setLineJoin",
            "setLineWidth",
            "scale",
            "setTitle",
        ]:
            raise AttributeError(attr)

        return getattr(self.ctx, attr)

    def setPageSize(self, width: Fraction, height: Fraction) -> None:
        self.ctx.setPageSize((width * cm, height * cm))

    def draw(self, flowable: Optional["Flowable"], x: float, y: float) -> None:
        if flowable:
            flowable.drawOn(self.ctx, x, y)

    def rect(
        self,
        left: Fraction,
        bottom: Fraction,
        width: Fraction,
        height: Fraction,
        *,
        fill: bool = True,
        stroke: bool = True,
    ) -> None:
        self.ctx.rect(left * cm, bottom * cm, width * cm, height * cm, fill=fill, stroke=stroke)

    def circle(
        self,
        left: Fraction,
        bottom: Fraction,
        radius: Fraction,
        *,
        fill: bool = True,
        stroke: bool = True,
    ) -> None:
        self.ctx.circle(left * cm, bottom * cm, radius * cm, fill=fill, stroke=stroke)

    def line(self, left: Fraction, bottom: Fraction, right: Fraction, top: Fraction) -> None:
        self.ctx.line(left * cm, bottom * cm, right * cm, top * cm)

    def polyline(self, points: list[tuple[Fraction, Fraction]]) -> None:
        p = self.ctx.beginPath()
        x, y = points[0]
        p.moveTo(x * cm, y * cm)
        for x, y in points[1:]:
            p.lineTo(x * cm, y * cm)
        self.ctx.drawPath(p)

    def drawImage(
        self,
        filename: str,
        left: Fraction,
        bottom: Fraction,
        width: Fraction,
        height: Fraction,
    ) -> None:
        self.ctx.drawImage(filename, left * cm, bottom * cm, width * cm, height * cm)

    def getvalue(self) -> bytes:
        self.showPage()
        self.ctx.save()
        return self.output.getvalue()

    @contextmanager
    def state(self, left: Fraction, bottom: Fraction, rotate: float) -> Iterator[None]:
        self.ctx.saveState()
        self.ctx.translate(left * cm, bottom * cm)
        self.ctx.rotate(rotate)

        yield

        self.ctx.restoreState()


class Flowable:
    def __init__(self, flowable: platypus.Flowable) -> None:
        self.flowable = flowable

    def wrap(self, width: Fraction, height: Fraction) -> tuple[Fraction, Fraction]:
        actual_width, actual_height = self.flowable.wrap(width * cm, height * cm)
        # TODO: would be nicer to have a more accurate conversion from float
        # that didn't result in weird denominators
        return Fraction(actual_width / cm), Fraction(actual_height / cm)

    def drawOn(self, ctx: canvas.Canvas, x: float, y: float) -> None:
        self.flowable.drawOn(ctx, x * cm, y * cm)


class Paragraph(Flowable):
    def __init__(self, content: str, alignment: Align, **style: Any) -> None:
        styles = getSampleStyleSheet()

        styles["Normal"].alignment = alignment.value

        for key, value in style.items():
            setattr(styles["Normal"], key, value)

        super().__init__(platypus.Paragraph(content, styles["Normal"]))


ColWidths = Sequence[Fraction]

TableDataType = list[Paragraph | str]
StyleType = (
    tuple[str, tuple[int, int], tuple[int, int]]
    | tuple[str, tuple[int, int], tuple[int, int], int | str]
    | tuple[str, tuple[int, int], tuple[int, int], int, Any]
)


class Table(Flowable):
    def __init__(self, data: list[TableDataType], colWidths: ColWidths) -> None:
        data2 = [[col.flowable if isinstance(col, Paragraph) else col for col in row] for row in data]
        super().__init__(platypus.Table(data2, colWidths=[col * cm for col in colWidths]))

    def setStyle(self, styles: list[StyleType]) -> None:
        self.flowable.setStyle(styles)
