from fractions import Fraction
from unittest.mock import Mock, call, patch

import pytest
from reportlab.lib import colors
from reportlab.lib.units import cm

from pdf_canvas import pdf
from pdf_canvas.pdf import Align


def test_get_color() -> None:
    assert pdf.get_color("black") == colors.Color(0, 0, 0, 1.0)
    assert pdf.get_color("black", 0.5) == colors.Color(0, 0, 0, 0.5)
    assert pdf.get_color("red") == colors.Color(1, 0, 0, 1.0)


def test_get_color_rgb() -> None:
    assert pdf.get_color_rgb("black") == (0, 0, 0)
    assert pdf.get_color_rgb("red") == (255, 0, 0)


def test_cm_to_pixels() -> None:
    # 10 cm = 3.937008 inches * 300 = 1181
    assert pdf.cm_to_pixels(10, 300) == 1181


@patch("pdf_canvas.pdf.canvas.Canvas")
def test_canvas(mock_canvas: Mock) -> None:
    canvas = pdf.Canvas(Fraction(10), Fraction(20))
    assert mock_canvas.mock_calls == [call(canvas.output, pagesize=(10 * cm, 20 * cm))]
    assert canvas.ctx == mock_canvas.return_value


@patch("pdf_canvas.pdf.canvas.Canvas")
def test_canvas_getattr(mock_canvas: Mock) -> None:
    canvas = pdf.Canvas(Fraction(10), Fraction(20))
    assert canvas.showPage == mock_canvas.return_value.showPage
    assert canvas.setStrokeColor == mock_canvas.return_value.setStrokeColor
    assert canvas.setFillColor == mock_canvas.return_value.setFillColor

    with pytest.raises(AttributeError):
        assert canvas.unknown


@patch("pdf_canvas.pdf.canvas.Canvas")
def test_canvas_setpagesize(mock_canvas: Mock) -> None:
    canvas = pdf.Canvas(Fraction(10), Fraction(20))
    canvas.setPageSize(Fraction(15), Fraction(25))
    assert mock_canvas.mock_calls == [
        call(canvas.output, pagesize=(10 * cm, 20 * cm)),
        call().setPageSize((15 * cm, 25 * cm)),
    ]


@patch("pdf_canvas.pdf.canvas.Canvas")
def test_canvas_draw(mock_canvas: Mock) -> None:
    flowable = Mock()
    canvas = pdf.Canvas(Fraction(10), Fraction(20))
    canvas.draw(flowable, 15, 25)
    assert flowable.mock_calls == [call.drawOn(mock_canvas.return_value, 15, 25)]


@patch("pdf_canvas.pdf.canvas.Canvas")
def test_canvas_rect(mock_canvas: Mock) -> None:
    canvas = pdf.Canvas(Fraction(10), Fraction(20))
    canvas.rect(Fraction(1), Fraction(2), Fraction(3), Fraction(4), fill=True, stroke=False)
    assert mock_canvas.mock_calls == [
        call(canvas.output, pagesize=(10 * cm, 20 * cm)),
        call().rect(1 * cm, 2 * cm, 3 * cm, 4 * cm, fill=True, stroke=False),
    ]


@patch("pdf_canvas.pdf.canvas.Canvas")
def test_canvas_circle(mock_canvas: Mock) -> None:
    canvas = pdf.Canvas(Fraction(10), Fraction(20))
    canvas.circle(Fraction(1), Fraction(2), Fraction(3), fill=True, stroke=False)
    assert mock_canvas.mock_calls == [
        call(canvas.output, pagesize=(10 * cm, 20 * cm)),
        call().circle(1 * cm, 2 * cm, 3 * cm, fill=True, stroke=False),
    ]


@patch("pdf_canvas.pdf.canvas.Canvas")
def test_canvas_line(mock_canvas: Mock) -> None:
    canvas = pdf.Canvas(Fraction(10), Fraction(20))
    canvas.line(Fraction(1), Fraction(2), Fraction(3), Fraction(4))
    assert mock_canvas.mock_calls == [
        call(canvas.output, pagesize=(10 * cm, 20 * cm)),
        call().line(1 * cm, 2 * cm, 3 * cm, 4 * cm),
    ]


@patch("pdf_canvas.pdf.canvas.Canvas")
def test_canvas_drawimage(mock_canvas: Mock) -> None:
    canvas = pdf.Canvas(Fraction(10), Fraction(20))
    canvas.drawImage("photo.jpg", Fraction(1), Fraction(2), Fraction(3), Fraction(4))
    assert mock_canvas.mock_calls == [
        call(canvas.output, pagesize=(10 * cm, 20 * cm)),
        call().drawImage("photo.jpg", 1 * cm, 2 * cm, 3 * cm, 4 * cm),
    ]


@patch("pdf_canvas.pdf.canvas.Canvas")
def test_canvas_getvalue(mock_canvas: Mock) -> None:
    canvas = pdf.Canvas(Fraction(10), Fraction(20))
    canvas.output.write(b"test")
    assert canvas.getvalue() == b"test"
    assert mock_canvas.mock_calls == [
        call(canvas.output, pagesize=(10 * cm, 20 * cm)),
        call().showPage(),
        call().save(),
    ]


@patch("pdf_canvas.pdf.canvas.Canvas")
def test_canvas_state(mock_canvas: Mock) -> None:
    canvas = pdf.Canvas(Fraction(10), Fraction(20))
    with canvas.state(Fraction(1), Fraction(2), 90):
        canvas.showPage()  # not realistic, but need a simple call

    assert mock_canvas.mock_calls == [
        call(canvas.output, pagesize=(10 * cm, 20 * cm)),
        call().saveState(),
        call().translate(1 * cm, 2 * cm),
        call().rotate(90),
        call().showPage(),
        call().restoreState(),
    ]


def test_flowable_wrap() -> None:
    flowable = Mock()
    flowable.wrap.return_value = (10 * cm, 20 * cm)
    assert pdf.Flowable(flowable).wrap(Fraction(15), Fraction(25)) == (
        Fraction(10),
        Fraction(20),
    )
    assert flowable.mock_calls == [call.wrap(15 * cm, 25 * cm)]


def test_flowable_drawon() -> None:
    flowable = Mock()
    ctx = Mock()
    pdf.Flowable(flowable).drawOn(ctx, 15, 25)
    assert flowable.mock_calls == [call.drawOn(ctx, 15 * cm, 25 * cm)]


@patch("pdf_canvas.pdf.platypus.Paragraph")
@patch("pdf_canvas.pdf.getSampleStyleSheet")
def test_paragraph(mock_getsamplestylesheet: Mock, mock_paragraph: Mock) -> None:
    styles = Mock()
    mock_getsamplestylesheet.return_value = {"Normal": styles}

    para = pdf.Paragraph("content", alignment=Align.Center, fontSize=10)

    assert mock_paragraph.mock_calls == [call("content", styles)]
    assert styles.fontSize == 10
    assert styles.alignment == 1

    assert para.flowable == mock_paragraph.return_value


@patch("pdf_canvas.pdf.platypus.Table")
def test_table(mock_table: Mock) -> None:
    table = pdf.Table([], [Fraction(1), Fraction(2)])

    assert mock_table.mock_calls == [call([], colWidths=[1 * cm, 2 * cm])]

    assert table.flowable == mock_table.return_value


@patch("pdf_canvas.pdf.platypus.Table")
def test_table_setstyle(mock_table: Mock) -> None:
    table = pdf.Table([], [Fraction(1), Fraction(2)])
    table.setStyle([])
    assert mock_table.mock_calls == [
        call([], colWidths=[1 * cm, 2 * cm]),
        call().setStyle([]),
    ]


@patch("pdf_canvas.pdf.platypus.Table")
@patch("pdf_canvas.pdf.platypus.Paragraph")
@patch("pdf_canvas.pdf.getSampleStyleSheet")
def test_table_paragraph(mock_getsamplestylesheet: Mock, mock_paragraph: Mock, mock_table: Mock) -> None:
    styles = Mock()
    mock_getsamplestylesheet.return_value = {"Normal": styles}

    table = pdf.Table([[pdf.Paragraph("content", Align.Center)]], [Fraction(1), Fraction(2)])

    assert mock_paragraph.mock_calls == [call("content", styles)]
    assert styles.alignment == 1
    assert mock_table.mock_calls == [call([[mock_paragraph.return_value]], colWidths=[1 * cm, 2 * cm])]

    assert table.flowable == mock_table.return_value
