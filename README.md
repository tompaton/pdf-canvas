# PDF Canvas

Wrapper for reportlab pdf generation for photo book pages.

[![PyPI - Version](https://img.shields.io/pypi/v/pdf-canvas.svg)](https://pypi.org/project/pdf-canvas)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/pdf-canvas.svg)](https://pypi.org/project/pdf-canvas)

-----

## Table of Contents

- [Installation](#installation)
- [License](#license)

## Installation

```console
pip install pdf-canvas
```

## License

`pdf-canvas` is distributed under the terms of the [MIT](https://spdx.org/licenses/MIT.html) license.
